from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def catalog_view(request):
    return HttpResponse('Це сторінка каталогу 📚')

def product_view(request):
    return HttpResponse('Це сторінка продукту 👗')