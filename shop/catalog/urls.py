from django.urls import path
from .views import *

urlpatterns = [
    # catalog/
    path('', catalog_view),
    # catalog/product/
    path('product/', product_view)
]